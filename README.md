Bad Apple!! for taskmgr
=======================

1. Run taskmgr
1. **View** > **Update speed** > **Paused**
1. Go to **Performance** > **CPU**
1. Right click graph > **Change graph to** > **Logical processors**
1. Right click graph > untick **Show kernel times**
1. Double click graph, then resize to your liking

```
> youtube-dl -o "%(id)s.%(ext)s" "https://www.youtube.com/watch?v=FtutLA63Cp8"
> ffmpeg -i FtutLA63Cp8.mp4 -vf scale=WIDTH:HEIGHT,hue=s=0 -r FPS -c:v rawvideo -pix_fmt rgb32 input.rgb
> rustup run x86_64-pc-windows-msvc cargo run --release -- input.rgb WIDTH HEIGHT FPS
```

Inspired by [@kbeckmann] and [@winocm], based on [bad_cpu.py], and
ported to Rust with a bunch of new features:

* support for more than 64 logical processors
* High Frame Rate™ beyond the traditional limit of 2 fps
* VM-friendly timing and support for dropping frames

For more details, check out [my blog post].

[@kbeckmann]: https://twitter.com/kbeckmann/status/1275835614529806348
[@winocm]: https://twitter.com/winocm/status/1276037359503466497
[bad_cpu.py]: https://gist.github.com/kbeckmann/41254cc559ee4917913e522cc529a4e5
[my blog post]: https://www.azabani.com/2020/06/29/bad-apple-for-taskmgr.html
