mod windows;

use std::convert::{TryFrom, TryInto};
use std::env::args;
use std::error::Error;
use std::ffi::OsString;
use std::fs::read;
use std::mem::{size_of, size_of_val};
use std::os::windows::ffi::OsStringExt;
use std::ptr::{null_mut as NULL};
use std::slice::from_raw_parts;
use std::sync::{Arc, Barrier, RwLock, atomic::{AtomicUsize, Ordering::Relaxed}};
use std::thread::sleep;
use std::time::{Duration, Instant};

use winapi::shared::basetsd::{DWORD_PTR, KAFFINITY};
use winapi::shared::minwindef::{BOOL, DWORD, LPVOID, FALSE, TRUE};
use winapi::shared::ntdef::{HANDLE, MAKELANGID, LANG_NEUTRAL, SUBLANG_DEFAULT};
use winapi::shared::winerror::ERROR_INSUFFICIENT_BUFFER;
use winapi::um::errhandlingapi::GetLastError;
use winapi::um::processthreadsapi::{PROC_THREAD_ATTRIBUTE_LIST, CreateRemoteThreadEx, CreateThread, GetCurrentProcess, InitializeProcThreadAttributeList, SetThreadPriority, UpdateProcThreadAttribute};
use winapi::um::sysinfoapi::{GetLogicalProcessorInformationEx};
use winapi::um::synchapi::{WaitForMultipleObjects};
use winapi::um::winbase::{INFINITE, FORMAT_MESSAGE_ALLOCATE_BUFFER, FORMAT_MESSAGE_FROM_SYSTEM, FORMAT_MESSAGE_IGNORE_INSERTS, THREAD_PRIORITY_IDLE, FormatMessageW};
use winapi::um::winnt::{GROUP_AFFINITY, LOGICAL_PROCESSOR_RELATIONSHIP, MAXIMUM_WAIT_OBJECTS, PROCESSOR_GROUP_INFO, SYSTEM_LOGICAL_PROCESSOR_INFORMATION, SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX};
use winapi::um::winuser::{INPUT, INPUT_KEYBOARD, KEYBDINPUT, KEYEVENTF_KEYUP, VK_F5, FindWindowW, SendInput, SetForegroundWindow};
use wio::wide::{FromWide, ToWide};

use crate::windows::{PinnedThread, die, buffer, get_group_affinities};

const DEPTH: usize = 4;

static mut START: Option<Instant> = None;
static mut READY: AtomicUsize = AtomicUsize::new(1);

struct PixelThread<'s> {
    phase_barrier: Arc<Barrier>,
    next_frame: Arc<RwLock<usize>>,
    settings: &'s Settings,
    pixel_index: usize,
    data: Vec<f32>,
}

struct ClockThread<'s> {
    phase_barrier: Arc<Barrier>,
    next_frame: Arc<RwLock<usize>>,
    settings: &'s Settings,
}

struct Settings {
    width: usize,
    height: usize,
    fps: f32,
    total_frames: usize,
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = args().skip(1);

    if args.len() < 4 {
        eprintln!("usage: badapple <input.rgb> <width> <height> <fps>");
        return Ok(());
    }

    let input = read(args.next().unwrap())?;
    let width = args.next().unwrap().parse()?;
    let height = args.next().unwrap().parse()?;
    let fps = args.next().unwrap().parse()?;
    let total_frames = input.len() / DEPTH / width / height;
    let settings = Settings { width, height, fps, total_frames };

    let mut pixel_affinities = get_group_affinities()?;
    let mut pixel_states = Vec::default();
    let mut pixel_threads = Vec::default();
    let phase_barrier = Arc::new(Barrier::new(pixel_affinities.len() + 1));
    let next_frame = Arc::new(RwLock::new(0));
    let mut clock_state = ClockThread {
        phase_barrier: phase_barrier.clone(),
        next_frame: next_frame.clone(),
        settings: &settings,
    };

    for i in 0..pixel_affinities.len() {
        pixel_states.push(PixelThread {
            phase_barrier: phase_barrier.clone(),
            next_frame: next_frame.clone(),
            settings: &settings,
            pixel_index: i,
            data: Vec::default(),
        });
    }

    // FIXME: allow partial canvas (e.g. 8x8 in top-left corner of 16x16)
    for frame in input.chunks_exact(DEPTH * settings.width * settings.height) {
        for (i, pixel) in frame.chunks_exact(DEPTH).enumerate() {
            // FIXME gamma?
            pixel_states[i].data.push(1.0 - f32::from(pixel[0]) / 255.0);
        }
    }

    unsafe {
        START = Some(Instant::now());
    }

    for (state, affinity) in pixel_states.iter_mut().zip(pixel_affinities) {
        pixel_threads.push(PinnedThread::new(
            affinity,
            THREAD_PRIORITY_IDLE,
            Some(pixel),
            state as *mut _ as *mut _,
        )?);
    }

    unsafe {
        let state: *mut _ = &mut clock_state;
        if CreateThread(NULL(), 0, Some(clock), state as *mut _, 0, NULL()) == NULL() {
            die();
        }
    }

    let handles = pixel_threads.iter().map(|x| x.handle).collect::<Vec<_>>();

    for handles in handles.chunks(MAXIMUM_WAIT_OBJECTS.try_into()?) {
        unsafe {
            if WaitForMultipleObjects(
                handles.len().try_into()?,
                handles.as_ptr(),
                TRUE,
                INFINITE,
            ) != 0 {
                die();
            }
        }
    }

    Ok(())
}

extern "system" fn pixel(state: LPVOID) -> DWORD {
    let state = state as *const PixelThread;

    loop {
        unsafe {
            // phase 1: read next_frame only
            (*state).phase_barrier.wait();
            if READY.fetch_add(1, Relaxed) == (*state).settings.width * (*state).settings.height {
                dbg!(START.unwrap().elapsed());
            }
            let i = *(*state).next_frame.read().unwrap();
            if i >= (*state).settings.total_frames {
                break;
            }
            let start = Instant::now();
            while start.elapsed() < Duration::from_secs_f32((*state).data[i] / (*state).settings.fps) {}
            // phase 2: write next_frame only
            (*state).phase_barrier.wait();
        }
    }

    0
}

extern "system" fn clock(state: LPVOID) -> DWORD {
    let state = state as *const ClockThread;

    unsafe {
        // ToWide doesn’t add a null terminator :c
        // (fixes intermittent ERROR_SEM_NOT_FOUND)
        let mut class = "TaskManagerWindow\0".to_wide();
        let handle = FindWindowW(&mut *class as *mut _ as *mut _, NULL());

        if handle == NULL() || SetForegroundWindow(handle) == FALSE {
            die();
        }

        // FIXME should try SendMessage, because this spams keys to any active window
        let mut down = INPUT { type_: INPUT_KEYBOARD, ..Default::default() };
        *down.u.ki_mut() = KEYBDINPUT { wVk: VK_F5.try_into().unwrap(), ..Default::default() };
        let mut up = INPUT { type_: INPUT_KEYBOARD, ..Default::default() };
        *up.u.ki_mut() = KEYBDINPUT { wVk: VK_F5.try_into().unwrap(), dwFlags: KEYEVENTF_KEYUP, ..Default::default() };
        let mut dropped_frames = 0;
        let mut last_log = 0.0;
        loop {
            // phase 1: read next_frame only
            (*state).phase_barrier.wait();
            let i = *(*state).next_frame.read().unwrap();
            if i >= (*state).settings.total_frames {
                break;
            }
            let until = (i + 1) as f32 / (*state).settings.fps;
            let now = START.unwrap().elapsed().as_secs_f32();
            let delta = until - now;
            let expected = i as f32 / (*state).settings.fps;
            let actual = now;
            let lateness = actual - expected;
            if now - last_log > 1.0 {
                last_log = now;
                println!(
                    "clock @ {}: time {:.3} (nominal {:.3}) sleeping {:.2e} (nominal {:.2e}) lateness {:.2e} (rate {:.2e}) dropped {}",
                    i,
                    actual, expected,
                    delta, 1.0 / (*state).settings.fps,
                    lateness, lateness / i as f32,
                    dropped_frames,
                );
            }
            if delta > 0.0 {
                sleep(Duration::from_secs_f32(delta));
            }
            if SendInput(1, &mut down, size_of_val(&down).try_into().unwrap()) == 0 {
                die();
            }
            if SendInput(1, &mut up, size_of_val(&up).try_into().unwrap()) == 0 {
                die();
            }
            // phase 2: write next_frame only
            (*state).phase_barrier.wait();
            let dropping = (lateness * (*state).settings.fps).max(0.0) as usize;
            *(*state).next_frame.write().unwrap() = i + 1 + dropping;
            dropped_frames += dropping;
        }
    }

    0
}
