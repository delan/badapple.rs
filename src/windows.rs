use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::ffi::OsString;
use std::fs::read;
use std::mem::{size_of, size_of_val};
use std::os::windows::ffi::OsStringExt;
use std::ptr::{null_mut as NULL};
use std::slice::from_raw_parts;
use std::sync::{Arc, Barrier, atomic::{AtomicUsize, Ordering::Relaxed}};
use std::thread::sleep;
use std::time::{Duration, Instant};

use winapi::ctypes::{c_int};
use winapi::shared::basetsd::{DWORD_PTR, KAFFINITY};
use winapi::shared::minwindef::{BOOL, DWORD, LPVOID, FALSE, TRUE};
use winapi::shared::ntdef::{HANDLE, MAKELANGID, LANG_NEUTRAL, SUBLANG_DEFAULT};
use winapi::shared::winerror::ERROR_INSUFFICIENT_BUFFER;
use winapi::um::errhandlingapi::GetLastError;
use winapi::um::processthreadsapi::{PROC_THREAD_ATTRIBUTE_LIST, CreateRemoteThreadEx, CreateThread, GetCurrentProcess, InitializeProcThreadAttributeList, SetThreadPriority, UpdateProcThreadAttribute};
use winapi::um::sysinfoapi::{GetLogicalProcessorInformationEx};
use winapi::um::synchapi::{WaitForMultipleObjects};
use winapi::um::minwinbase::{LPTHREAD_START_ROUTINE};
use winapi::um::winbase::{INFINITE, FORMAT_MESSAGE_ALLOCATE_BUFFER, FORMAT_MESSAGE_FROM_SYSTEM, FORMAT_MESSAGE_IGNORE_INSERTS, THREAD_PRIORITY_IDLE, FormatMessageW};
use winapi::um::winnt::{GROUP_AFFINITY, LOGICAL_PROCESSOR_RELATIONSHIP, MAXIMUM_WAIT_OBJECTS, PROCESSOR_GROUP_INFO, SYSTEM_LOGICAL_PROCESSOR_INFORMATION, SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX};
use winapi::um::winuser::{INPUT, INPUT_KEYBOARD, KEYBDINPUT, KEYEVENTF_KEYUP, VK_F5, SendInput};
use wio::wide::FromWide;

#[allow(non_upper_case_globals)]
const RelationGroup: LOGICAL_PROCESSOR_RELATIONSHIP = 4;
#[allow(non_upper_case_globals)]
const RelationAll: LOGICAL_PROCESSOR_RELATIONSHIP = 0xFFFF;

const PROC_THREAD_ATTRIBUTE_NUMBER: DWORD_PTR = 0x0000FFFF;
const PROC_THREAD_ATTRIBUTE_THREAD: DWORD_PTR = 0x00010000;
const PROC_THREAD_ATTRIBUTE_INPUT: DWORD_PTR = 0x00020000;
const PROC_THREAD_ATTRIBUTE_ADDITIVE: DWORD_PTR = 0x00040000;
const ProcThreadAttributeGroupAffinity: DWORD_PTR = 3;

fn ProcThreadAttributeValue(Number: DWORD_PTR, Thread: bool, Input: bool, Additive: bool) -> DWORD_PTR {
    return Number & PROC_THREAD_ATTRIBUTE_NUMBER
    | if Thread { PROC_THREAD_ATTRIBUTE_THREAD } else { 0 }
    | if Input { PROC_THREAD_ATTRIBUTE_INPUT } else { 0 }
    | if Additive { PROC_THREAD_ATTRIBUTE_ADDITIVE } else { 0 };
}

fn PROC_THREAD_ATTRIBUTE_GROUP_AFFINITY() -> DWORD_PTR {
    ProcThreadAttributeValue(ProcThreadAttributeGroupAffinity, true, true, false)
}

pub(crate) struct PinnedThread {
    pub handle: HANDLE,

    // FIXME do these need Pin or similar?

    // CreateRemoteThreadEx needs(?) stable lpAttributeList
    attributes: Vec<u8>,

    // UpdateProcThreadAttribute needs stable lpValue
    affinity: Box<GROUP_AFFINITY>,
}

impl PinnedThread {
    pub fn new(affinity: GROUP_AFFINITY, priority: DWORD, fun: LPTHREAD_START_ROUTINE, parameter: LPVOID) -> Result<Self, Box<dyn Error>> {
        let mut affinity = Box::new(affinity);

        let mut attributes = unsafe {
            buffer(|buffer, len| InitializeProcThreadAttributeList(buffer, 1, 0, len))?
        };

        if unsafe {
            UpdateProcThreadAttribute(
                attributes.as_mut_ptr() as *mut _,
                0,
                PROC_THREAD_ATTRIBUTE_GROUP_AFFINITY(),
                &mut *affinity as *mut _ as *mut _,
                size_of_val(&*affinity),
                NULL(),
                NULL(),
            )
        } == FALSE {
            die();
        }

        let handle = unsafe {
            CreateRemoteThreadEx(
                GetCurrentProcess(),
                NULL(),
                0,
                fun.into(),
                parameter,
                0,
                &mut *attributes as *mut _ as *mut _,
                NULL()
            )
        };

        if handle == NULL() {
            die();
        }

        if unsafe {
            // FIXME winapi crate uses DWORD for signed priority constants
            SetThreadPriority(handle, THREAD_PRIORITY_IDLE as c_int)
        } == FALSE {
            die();
        }

        Ok(Self { handle, attributes, affinity })
    }
}

pub(crate) fn die() -> ! {
    unsafe {
        let mut pointer = NULL();

        FormatMessageW(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL(),
            GetLastError(),
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT).into(),
            &mut pointer as *mut _ as *mut _,
            0,
            NULL(),
        );

        panic!("{:?}", OsString::from_wide_ptr_null(pointer));
    }
}

pub(crate) unsafe fn buffer<F: FnMut(*mut T, *mut L) -> BOOL, T, L, E>(mut fun: F) -> Result<Vec<u8>, Box<dyn Error>>
where L: Default + Copy + TryInto<usize, Error = E>, E: Error + 'static {
    let mut result = Vec::default();
    let mut len: L = Default::default();

    loop {
        if fun(result.as_mut_ptr() as *mut T, &mut len) != FALSE {
            result.set_len(len.try_into()?);
            return Ok(result);
        } else if GetLastError() == ERROR_INSUFFICIENT_BUFFER {
            result.reserve(len.try_into()?);
        } else {
            die();
        }
    }
}

pub(crate) fn get_group_affinities() -> Result<Vec<GROUP_AFFINITY>, Box<dyn Error>> {
    let mut result = Vec::default();

    for (group, mask) in get_group_masks()?.iter().enumerate() {
        for i in 0..(8 * size_of::<KAFFINITY>()) {
            let Mask = mask & 1 << i;
            if Mask != 0 {
                result.push(GROUP_AFFINITY {
                    Mask,
                    Group: group.try_into()?,
                    ..Default::default()
                });
            }
        }
    }

    Ok(result)
}

fn get_group_masks() -> Result<Vec<KAFFINITY>, Box<dyn Error>> {
    let mut result = Vec::default();
    let records = unsafe {
        buffer(|buffer, len| GetLogicalProcessorInformationEx(RelationGroup, buffer, len))?
    };

    // println!("{}", records.len());

    let mut offset = 0;
    let mut record = records[offset..].as_ptr() as *const SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX;

    loop {
        let size = unsafe {
            usize::try_from((*record).Size)?
        };

        if size == 0 || offset + size > records.len() {
            break;
        }

        unsafe {
            println!("record: Relationship {} Size {}", (*record).Relationship, (*record).Size);

            if (*record).Relationship == 4 {
                let record = (*record).u.Group();
                println!("        MaximumGroupCount {} ActiveGroupCount {}", record.MaximumGroupCount, record.ActiveGroupCount);

                let groups = from_raw_parts(record.GroupInfo.as_ptr(), usize::from(record.ActiveGroupCount));

                for group in groups {
                    println!("                MaximumProcessorCount {} ActiveProcessorCount {}", group.MaximumProcessorCount, group.ActiveProcessorCount);
                    println!("                ActiveProcessorMask {:064b}", group.ActiveProcessorMask);
                    result.push(group.ActiveProcessorMask);
                }
            }
        }

        offset += size;
        record = records[offset..].as_ptr() as *const SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX;
    }

    Ok(result)
}
